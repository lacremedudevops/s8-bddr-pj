

SELECT print_notice('-- Creating View_CreneauxActuels');
DROP VIEW IF EXISTS View_CreneauxActuels;
-- Liste les cours en train d'être enseignés en ce moment même
-- Très pratique pour récupérer le cours actuel d'un prof avec l'id_prof, ou le cours actuel d'une salle avec id_salle
CREATE VIEW View_CreneauxActuels AS
	SELECT id, label,
		heure_debut, heure_fin,
		expiration_minutes, code_secret,
		id_enseignement, id_salle,
		id_prof, id_sharding_promo
	FROM CreneauFormation creneau
	WHERE 
	-- (Optimisation) Fraction mensuelle : ce jour-ci
	creneau.jour >= current_date AND creneau.jour < current_date + interval '1 day'
	-- (Optimisation) Cours actuel : tout de suite pour le prof #1
	AND creneau.heure_debut <= now() AND creneau.heure_fin >= now();

-- Exemple : 
-- Obtenir le cours actuel pour le prof 6
EXPLAIN SELECT * from View_CreneauxActuels
	WHERE id_prof=6;
-- Task > append > index scan sur creneauformation_p2024_05_104441
-- Optimisation :   11 sous-plans supprimés


-- Liste les signatures du jour
-- Très pratique pour récupérer les signatures du jour pour une classe donnée, filtrable par id_classe
SELECT print_notice('-- Creating View_SignaturesAujd');
DROP VIEW IF EXISTS View_SignaturesAujd;
CREATE VIEW View_SignaturesAujd AS
SELECT eleve.id as id_eleve, eleve.id_classe, eleve.id_sharding_promo as id_sharding_promo,
	person.prenom, person.nom, 	
	presence.id as id_presence, presence.id_event,
	presence.jour, presence.date_signature,
	presence.signature_png, presence.moyen_signature
FROM utilisateur_eleve eleve
	INNER JOIN utilisateur_base person USING (id)
	LEFT JOIN Presence on eleve.id = presence.id_utilisateur
		AND eleve.id_sharding_promo = presence.id_sharding_promo
		AND presence.jour >= now() + interval '-1 day';

-- signatures du jour pour la LSI 1
SELECT * from View_SignaturesAujd
WHERE id_classe = 1;