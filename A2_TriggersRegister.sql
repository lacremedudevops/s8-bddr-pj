

SET LOCAL citus.multi_shard_modify_mode TO 'sequential';
-- À appeler tous les ans
-- Exécute les procédures de maintenance pour l'année scolaire 2023
CALL maintenance_partitionner_annee_scolaire(2023);

-- À appeler tous les ans
-- Supprime les données archivées de l'année scolaire 2018
CALL maintenance_supprimer_donnnees_anciennes(2023 - 5);

BEGIN TRANSACTION;
-- Enregistrement du trigger update_classe_effectif sur tous les shards 
SELECT print_notice('-- Creating trigger update_classe_effectif_trigger');
SELECT run_command_on_colocated_placements(
  'Utilisateur_Eleve',
  'Classe',
  $cmd$
    CREATE OR REPLACE TRIGGER update_classe_effectif_trigger
	AFTER INSERT OR UPDATE OR DELETE ON %s
      FOR EACH ROW EXECUTE FUNCTION update_classe_effectif(%L)
  $cmd$
);
COMMIT;

BEGIN TRANSACTION;
-- Enregistrement du trigger valider_creneau_data sur tous les shards
SELECT print_notice('-- Creating trigger valider_creneau_data_trigger');
SELECT run_command_on_shards(
  'CreneauFormation',
  $cmd$
    CREATE OR REPLACE TRIGGER valider_creneau_data_trigger
	BEFORE INSERT OR UPDATE ON %s
      FOR EACH ROW EXECUTE FUNCTION valider_creneau_data()
  $cmd$
);
COMMIT;
