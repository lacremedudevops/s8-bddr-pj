# SomaSign

Projet de base de données réparties.

Logane TANN, Jean Louis CHEN, LSI 1

### 1. Choix du Secteur et Analyse des Besoins

*Sélectionnez un secteur d'activité pertinent et analysez les besoins spécifiques en matière de données.*

> Notre projet : un clone de l'application So We Sign sur mesure pour l'EFREI.
> Dans l'espoir d'avoir une solution moins chère et mieux adaptée aux formations de l'EFREI

> Secteur : domaine de la formation, projet de type "application interne" donc besoin très spécifique (contrairement à des applications de type SaaS qui visent la généralité et le grand nombre d'utilisateurs).  
> Il faut que les collaborateurs puissent indiquer leur présence de manière fiable, au moyen d'un émargement.

*Identifiez les exigences en termes de volume de données, de fréquence d'accès, de répartition géographique des utilisateurs et de contraintes réglementaires.*

> Volume de données : 
> - Volumétrie de taille normale, évoluant linéairement en fonction du nombre d'élèves
>   - 1 émargement par élève et par créneau horaire
> - Durée de vie maximale des données : 5 ans

> Fréquence d'accès : 
> - Forte charge à des moments précis de la journée (ex : début d'un cours)
> - Très faible charge sur les données anciennes
> - La latence n'est pas un critère important.
> - En revanche, la disponibilité est cruciale.
> 
> -> Ainsi, **nous sommes plus dans une configuration de type "timeseries" en comparaison à une configuration type "multi-tenant"**

> Répartition géographique : 
> - Seulement la france, car besoin spécifique interne à l'EFREI.
> - Au vu de notre besoin, un seul cluster citus est nécessaire.

### 2. Conception de l'Architecture de la Base de Données

*Déterminez les modalités de fragmentation verticale, horizontale ou mixte adaptées aux cas d'utilisation du secteur.*

> #### Fragmentation horizontale
> 
> Au vu de notre besoin et du modèle de données :  
> -> La donnée qui sera la plus accédée est l'émargement pour un cours.  
> -> Nous pensons qu'un partionnement par promotion (FESE-2025, FISE-2025, FISA-2025...) semble être le choix le plus efficace. Nous utiliserons `create_distributed_table()` pour cela.  
> -> Nous allons en plus partitionner par timeserie avec `create_time_partitions()`. La donnée la plus récente sera celle avec le plus de fréquence d'accès.  
>
> #### Fragmentation verticale
> (distribution des données avec moins d'accès sur des tables différentes)

*Planifiez la répartition des nœuds Citus sur différents data centers ou régions cloud pour une distribution géographique*

> Comme argumenté précédemment, une distribution géographique n'est pas jugée nécessaire. Nous avons eu aussi des difficultés à faire fonctionner deux clusters sur docker compose.

### 3. Implémentation et Configuration :

#### Mettez en place l'infrastructure avec Oracle ou PostgreSQL et Citus, en configurant les nœuds et les règles de distribution des données.

> Suite à l'épuisement de nos crédits azure, nous avons configuré un cluster citus doté de 4 noeuds sur Docker-compose.
> 
> ```shell
> $ docker compose down -v
> $ docker compose up
> ```
> 
> Se connecter avec pgadmin `somasign:soma@localhost:5555` et exécuter les scripts SQL par ordre alphabétique.
> 
> Attention : exécuter au moins les scripts A0, A1 et A2 avant de toucher aux données !

#### Tables créées 

![liste des tables](img/Tables.png)


![Diagramme des classes](BDDR.drawio.png)

### 3 bis. Question de recherche

(PostgreSQL) : Étude des options de réplication disponibles dans Azure Database for PostgreSQL Hyperscale (Citus) :

- Quelles sont les stratégies de réplication intégrées fournies par Azure pour PostgreSQL Hyperscale?

Azure propose plusieurs stratégies disponibles :

  -> **Sharding** : Citus divise les données en plusieurs partitions et distribue ces "shards" sur plusieurs noeuds dans le cluster. Chaque noeuds contient une partie des données, ce qui permet d'améliorer les performances pour les requêtes parallèles.

  -> **Shard Replication** : Citus réplique les partitions en plusieurs exemplaires et les distribue sur différents noeuds dans le cluster. Cela permet d'assurer la disponibilité des données en cas de panne sur un noeud par exemple.

  -> **Global Index Replication** : Citus propose des noeuds coordonnateurs, permettant des requêtes qui nécessitent des opérations sur des données globales.

  -> **Read Replicas** : Azure propose de créer des réplicas en lecture seule, qui peuvent être utiles pour répartir la charge des requêtes de lecture des noeuds principaux. Cela permet d'augmenter la capacité de lecture.

  -> **Automated Backups and Point-In-Time Restore** : Une sauvegarde automatique et restoration à un point dans le temps. Ces sauvegardes peuvent elles même être répliquées et stockés de manière redondante pour assurer leur disponibilité.

  -> **Geo-Redundancy** : C'est une extension de la réplication standard. Cela permet d'avoir des réplicas de la base de données à des régions géographiques différentes. Toujours pour une résilience, mais aussi une réduction de la latence.


- Comment ces stratégies peuvent-elles être personnalisées pour répondre à des scénarios de haute disponibilité spécifiques?

 -> **sharding** : Nous pouvons par exemple personnaliser les colonnes de distributions spécifiques pour optimiser les performances. Aussi, il est possible de personnaliser le nombre de répliques pour chaque shard, ce qui permet de cibler les shards nécessaires aux applications critiques et d'avoir une résilience encore plus élevé (mais coûte plus de ressources)/

 -> **Automated Backups and Point-In-Time Restore** : Concernant les sauvegardes, nous pouvons configurer la fréquence et la rétention (temps avant que la donnée soit effacé) des sauvegardes automatiques. Cela nous permet de minimiser la perte de donné en cas d'accident.

 -> **Geo-Redundancy** : En déployant une configuration multi-régionale, nous permettons une haute disponibilité géographique. En cas de panne régionale, les répliques de d'autres régions peuvent toujours prendre le relais.


### 4. Questions de fonctionnalités spécifiques

Créez des vues, procédures stockées et triggers qui correspondent aux besoins métier du secteur.

#### Procédure de maintenance `maintenance_partitionner_annee_scolaire(year_input INT)`

Effectue la création des tables de partition pour une année scolaire

| Avant | Après |
| --- | --- |
| ![Liste des tables](img/Tables.png) | ![Liste des tables fractionnées sur la durée](img/TimePartition.png) |


#### Trigger distribué `update_classe_effectif_trigger`

Mets à jour Classe.effectif à l'insertion / modification / suppression d'un élève. Trigger s'exécutant sur toutes les tables distribuées

![comparatif avant / après du trigger](img/trigger_count_eleves.png)

#### Trigger distribué `valider_creneau_data`

Empêche l'ajout d'un créneau dans une salle possédant déjà un cours enregistré.. Trigger s'exécutant sur toutes les tables distribuées

![erreur lorsque la salle est déjà réservée](img/salle_reservee.png)

#### Vue distribuée `View_CreneauxActuels`

Affiche la liste des cours qui se déroulent en ce moment même. Optimisé avec fractionnement temporel et sharding.

![résultats View_CreneauxActuels](img/view_creneaux_actuels.png)

#### Vue distribuée `View_SignaturesAujd`

Affiche tous les élèves et leurs signatures du jour. Optimisé avec le fractionnement temporel et sharding

![Résultat de la requête pour LSI 1](img/view_sign.png)

*On remarque ici que tous les évèves de la LSI 1 sont affichés, mais qu'on peut savoir s'ils ont signé ou non.*

#### Procédure de maintenance `maintenance_supprimer_donnnees_anciennes(current_year INT)`

Les promotions sont dotées d'une date de suppression `deletion_year`, généralement définie à (date_creation + 5 ans).

Cette procédure supprime les promotions ainsi que toutes les données liées dont la `deletion_year` est inférieure à la date spécifiée en paramètre.

| Erreur : pas de promos dont `deletion_date` < `current_year`  | Suppression des données lorsque `current_year = 2030` |
| --- | --- |
| ![Suppression 2023](img/suppression_err.png) | ![Suppression 2030](img/suppr_succ.png) |

### 4 bis. Question de recherche

Étude des mécanismes de sécurité d'Azure pour PostgreSQL et Citus :

- Quelles sont les pratiques de sécurité par défaut fournies par Azure pour les bases de données PostgreSQL avec Citus ?

Azure propose par défaut des outils pour sécuriser nos bases de données :

-> **Azure Virtual Network** : Nous pouvons déployer notre instance Citus dans une Azure Virtual Network. Cela permet de contrôler l'accès réseau à la base de données, notamment avec le pare-feu.

-> **Azure Private Link** : Relié au point précédent, Azure Private Link permet de nous connecter à notre base Citus depuis notre réseau virtuel. Permettant d'éliminer l'exposition publique à Internet.

-> **Chiffrement des données** : Azure chiffre automatiquement les données au repos à l'aide de clés de chiffrement Azure Key Vault. Pour les données en transit, Azure utilie SSL/TLS pour les chiffrer.

-> **Authentification** : L'authentification se base sur Azure Active Directory (AAD) qui permet une centralisation des identités et politiques d'accès.

-> **Sauvegarde** : Comme précedemment, nous pouvons réaliser des sauvegardes automatiques des bases de données Citus. Ensuite, nous pouvons les restaurer à un point donné dans le temps après un incident par exemple.

-> **Audit** : Azure propose des fonctionnalités d'audit pour suivre les activités et accès à la base Citus. De plus, Azure Security Center fournit une surveillance continue pour détecter les menaces ou vulnérabilités.

- Comment ces pratiques peuvent-elles être évaluées ou améliorées pour garantir la conformité avec le RGPD ?

Les audits et la surveillance permet de monitorer notre base de données. La MFA ajoute une couche de sécurité.

La sauvegarde des données doit avoir une date limite de rétention. Les données stockées doivent rester en europe.

On a implémenté le schéma pour que les comptes aient le champ `expiration_date`.

Idem pour la promo, le champ `archive_date` et `deletion_date` sont renseignés. Le trigger de maintenance s'occupe de supprimer toutes les données liées à la promotion dès que la deletion_date est dépassée.

Sinon, pour garantir la conformité, il suffit de respecter les principes de base du RGPD. Par ex. ne stocker que les données nécessaires, ajouter du chiffrement avant stockage et ajouter une politique d'archivage / suppression automatique des données.

### 5. Optimisation et Tests :

Optimisez les performances des requêtes et réduisez la latence en utilisant des outils de profilage et de benchmarking.

Exécutez des tests de charge pour évaluer la capacité du système à gérer de gros volumes de transactions simultanées.

> Utilisé : 
> - Distribution par promotion et partitionnement par mois (cf illustrations plus haut et script sql A0)
> - Requêtes conçues pour supporter la distribution (cf illustrations plus haut et script SQL des vues)
> - étude de la performance avec EXPLAIN (cf. illustration partie `View_CreneauxActuels`)
> - Pas de simulation de charge en vue du MDD trop complexe (cf. feedback partie 7)
> - jointure parallèles : géré par citus automatiquement.

### 6. Documentation

Rédigez une documentation technique (Scripts SQL) complète pour l'administration du système et la gestion des données.

> Documentation présente dans les scripts SQL.

### 7. Feedback / Conclusion

Ce projet nous a permis de comprendre l'utilité des bases de données réparties.

Points positifs : 
- Citus est très puissant pour la gestion de la répartition de la donnée !  
  Il gère à notre place le fractionnement horizontal des tables.
  Il contient plusieurs fonctions utilitaires pratiques (tel qu'exécuter une commande sur tous les shards)

Points négatifs : 
- Citus n'était PAS du tout adapté à notre cas (application interne).  
  - Notre schéma de données était hautement relationnel, cela fonctionne mal avec le modèle en "timeseries". Les requêtes étaient très difficiles à construire.
  - Difficulté pour créer de fausses données, nécessaire pour faire du benchmark
  - Difficulté dans l'ajout de contraintes relationnelles
- Nous n'avons pas réussi à créer deux clusters sur docker, et la solution proposée sur moodle ne nous a pas donné assez de marge niveau temps. Nous avons épuisé nos crédits étudiants azure.
- -> Nous avons découvert trop tard qu'une base de données avec FDW, sans sharding géré citus, aurait largement suffi. Des triggers pour assurer la maintenance et la réplication des données auraient été une bonne solution.
- Tuncate des tables lent, trop de sharding pour avoir une performance satisfaisante au vu du faible volume de données.
