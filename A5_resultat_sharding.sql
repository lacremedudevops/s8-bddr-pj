-- lister les workers
SELECT * FROM citus_get_active_worker_nodes();

-- lister les différentes tables
SELECT table_name, citus_table_type, colocation_id, distribution_column, shard_count, access_method
FROM citus_tables
ORDER BY colocation_id, table_name;