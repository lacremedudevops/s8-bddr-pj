-- Création des tables de partition citus pour une année scolaire donnée.
-- Ex : maintenance_partitionner_annee_scolaire(2023) : Crée une partition de aout 2023 à juillet 2024
CREATE OR REPLACE PROCEDURE maintenance_partitionner_annee_scolaire(year_input INT)
AS $$
BEGIN
    DECLARE
        start_date TIMESTAMP := (year_input || '-08-01')::TIMESTAMP;
    BEGIN
        PERFORM print_notice(format('-- Paritionning CreneauFormation and Presence for year %s...', year_input));
        PERFORM create_time_partitions(
            table_name         := 'CreneauFormation',
            partition_interval := '1 month',
            start_from         := start_date,
            end_at             := start_date + '12 months'
        );
        PERFORM create_time_partitions(
            table_name         := 'Presence',
            partition_interval := '1 month',
            start_from         := start_date,
            end_at             := start_date + '12 months'
        );
        PERFORM print_notice(format('-- Created time partition for year %s', year_input));
    END;
END;
$$ LANGUAGE plpgsql;


-- Supprime les données archivées de l'année scolaire spécifiée en paramètres
-- Ex : maintenance_supprimer_donnnees_anciennes(2023) : Supprime les données de aout 2023 à juillet 2024
CREATE OR REPLACE PROCEDURE maintenance_supprimer_donnnees_anciennes(current_year INT)
AS $$
BEGIN
    DECLARE
        deletion_year TIMESTAMP := (current_year || '-08-01')::TIMESTAMP;
        can_delete INT;
    BEGIN
        SELECT COUNT(*) INTO can_delete FROM Promotion
        WHERE deletion_date < deletion_year;

        IF can_delete <= 0 THEN
            RAISE NOTICE 'No data to delete before year %.', current_year;
            RETURN;
        END IF;

        DELETE FROM Presence
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM CreneauFormation
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM Enseignement
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM Utilisateur_Account
        WHERE Utilisateur_Account.id IN
			(SELECT Id from Utilisateur_Eleve
				WHERE id_sharding_promo IN (
              SELECT id FROM Promotion WHERE deletion_date < deletion_year
	          )
		);
        DELETE FROM Utilisateur_Base
        WHERE Utilisateur_Base.id IN
			(SELECT Id from Utilisateur_Eleve
				WHERE id_sharding_promo IN (
              SELECT id FROM Promotion WHERE deletion_date < deletion_year
	          )
		);
        DELETE FROM Utilisateur_Eleve
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM Classe
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM Matiere
        WHERE id_sharding_promo IN (
            SELECT id FROM Promotion WHERE deletion_date < deletion_year
        );
        DELETE FROM Promotion
        WHERE deletion_date < deletion_year;
    END;
END;
$$ LANGUAGE plpgsql;


-- Crée un trigger sur chaque shard de la table pour mettre à jour l'effectif de la classe
-- Ex: update_classe_effectif('Classe_xxxx') où Classe_xxxx est la table Classe colocated à la table Utilisateur_Eleve
SELECT print_notice('-- Creating trigger update_classe_effectif');
CREATE OR REPLACE FUNCTION update_classe_effectif()
RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.id_classe <> OLD.id_classe) THEN
		EXECUTE format(
            '
            UPDATE %s
            SET effectif = effectif + 1
            WHERE id = $1 AND id_sharding_promo = $2;
            ',
            TG_ARGV[0]
        ) USING NEW.id_classe, NEW.id_sharding_promo;
    END IF;
    IF TG_OP = 'DELETE' OR (TG_OP = 'UPDATE' AND NEW.id_classe <> OLD.id_classe) THEN
		EXECUTE format(
            '
            UPDATE %s
            SET effectif = effectif - 1
            WHERE id = $1 AND id_sharding_promo = $2;
            ',
            TG_ARGV[0]
        ) USING OLD.id_classe, OLD.id_sharding_promo;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Crée un trigger sur chaque shard de la table pour vérifier que le créneau est
-- cohérent et que la salle n'est pas déjà réservée
-- Ex: valider_creneau_data()
SELECT print_notice('-- Creating trigger valider_creneau_data');
CREATE OR REPLACE FUNCTION valider_creneau_data()
RETURNS TRIGGER AS $$
DECLARE
    id_classe INT;
    creneau_count INT;
BEGIN
    -- Vérifie la cohérence du créneau
    IF NEW.heure_debut >= NEW.heure_fin THEN
        RAISE EXCEPTION 'Heure de début doit être avant l''heure de fin';
    END IF;
    -- Définis automatiquement le champ jour
    NEW.jour := date_trunc('day', NEW.heure_debut);
    -- Vérifie si la salle est déjà réservée
    EXECUTE format(
        '
        SELECT COUNT(*) FROM %s
        WHERE id_sharding_promo = $1
        AND id_salle = $2
        AND ( 
            (heure_fin >= $3 AND heure_fin <= $4) OR (heure_debut >= $5 AND heure_debut <= $6)
        );
        ',
        TG_TABLE_NAME
    ) USING NEW.id_sharding_promo, NEW.id_salle, NEW.heure_debut, NEW.heure_fin, NEW.heure_debut, NEW.heure_fin
	INTO creneau_count;
        
    IF creneau_count > 0 THEN
        RAISE EXCEPTION 'Cette salle est déjà réservée.';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
