
SELECT print_notice('-- Clearing data');
TRUNCATE Presence,
    CreneauFormation,
    Enseignement,
    Utilisateur_Eleve,
    Classe,
    Matiere,
    Utilisateur_Account,
    Utilisateur_Prof,
    Utilisateur_Base,
    Promotion,
    Salle;

SELECT print_notice('-- Injecting data');
INSERT INTO Salle (id, nom_batiment, nom_salle) VALUES
(1, 'Home', 'H-202'),
(2, 'Home', 'H-203');

INSERT INTO Promotion (id, code, full_name, lock_date, deletion_date)
VALUES (1, 'FISA-2025', 'Formation Ingénieur / Statut Apprenti / Diplôme 2025', '2026-01-01 00:00:00', '2030-01-01 00:00:00');

INSERT INTO Utilisateur_Base (id, type_utilisateur, prenom, nom)
VALUES (1, 'eleve', 'Logane', 'TANN'),
(2, 'eleve', 'eleve2', 'nom2'),
(3, 'eleve', 'eleve3', 'nom3'),
(4, 'eleve', 'eleve4', 'nom4'),
(5, 'eleve', 'eleve5', 'nom5'),
(6, 'prof', 'prof1', 'nom1'),
(7, 'prof', 'prof2', 'nom2');


INSERT INTO Utilisateur_Prof (id, salaire_horaire, is_externe)
VALUES (6, 50, FALSE),
(7, 50, TRUE);

INSERT INTO Utilisateur_Account (id, email, mdp, expiration_date)
VALUES (1, 'user1@email.fr', 'password1', '2025-01-01 00:00:00'),
(2, 'user2@email.fr', 'password2', '2025-01-01 00:00:00'),
(3, 'user3@email.fr', 'password3', '2025-01-01 00:00:00'),
(4, 'user4@email.fr', 'password4', '2025-01-01 00:00:00'),
(5, 'user5@email.fr', 'password5', '2025-01-01 00:00:00'),
(6, 'user6@email.fr', 'password6', '2025-01-01 00:00:00'),
(7, 'user7@email.fr', 'password7', '2025-01-01 00:00:00');

INSERT INTO Matiere (id, nom_matiere, id_sharding_promo)
VALUES (1, 'Base de données réparties', 1),
    (2, 'Architecture SI', 1);
    

INSERT INTO Classe(id, nomClasse, effectif, id_sharding_promo) VALUES
(1, 'M1 APP LSI 1', 0, 1),
(2, 'M1 APP LSI 2', 0, 1);

INSERT INTO Utilisateur_Eleve (id, id_classe, id_sharding_promo)
VALUES (1, 1, 1),
(2, 1, 1),
(3, 2, 1),
(4, 2, 1),
(5, 2, 1);

SELECT * from Classe
WHERE id_sharding_promo = 1;
-- id   nomclasse       effectif    id_sharding_promo
-- 1	"M1 APP LSI 1"	2	1
-- 2	"M1 APP LSI 2"	3	1

INSERT INTO CreneauFormation
    (id, label, jour, heure_debut, heure_fin, id_enseignement, id_salle, expiration_minutes, code_secret, id_prof, id_sharding_promo) VALUES
    (110, 'Test temps réel', now() + interval '-1 minute', now() + interval '-1 minute', now() + interval '24 hour', 1, 1, 5, 1234, 6, 1),
    (112, 'Test temps réel', now() + interval '-1 minute', now() + interval '-1 minute', now() + interval '24 hour', 4, 2, 5, 1234, 7, 1);

INSERT INTO Presence
    (id, id_utilisateur, id_event, jour, date_signature, signature_png, moyen_signature, id_sharding_promo) VALUES
    (110, 1, 110, now(), now(), 'url1', 'manuel', 1),
    (112, 4, 112, now(), now(), 'url1', 'manuel', 1);
