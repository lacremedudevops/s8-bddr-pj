-- Les nodes et les utilisateurs sont déjà configurés par docker compose.
-- Activons l'extension citus au cas où
CREATE EXTENSION IF NOT EXISTS citus;

-- debug
CREATE OR REPLACE FUNCTION print_notice(msg text) 
  RETURNS integer AS 
$$ 
DECLARE 
BEGIN 
    RAISE NOTICE USING MESSAGE = msg;
    RETURN null;
END; 
$$ 
LANGUAGE 'plpgsql' IMMUTABLE; 

-- Reset des tables
DROP TABLE IF EXISTS Presence;
DROP TABLE IF EXISTS CreneauFormation;
DROP TABLE IF EXISTS Enseignement;
DROP TABLE IF EXISTS Utilisateur_Eleve;
DROP TABLE IF EXISTS Classe;
DROP TABLE IF EXISTS Matiere;
DROP TABLE IF EXISTS Utilisateur_Account;
DROP TABLE IF EXISTS Utilisateur_Prof;
DROP TABLE IF EXISTS Utilisateur_Base;
DROP TABLE IF EXISTS Promotion;
DROP TABLE IF EXISTS Salle;

-- Reset des enums
DROP TYPE IF EXISTS TypeSignature;
DROP TYPE IF EXISTS TypeUtilisateur;

-- Définit le type utilisateur
-- 'prof' -> L'Utilisateur_Base a un Utilisateur_Prof associé
-- 'eleve' -> L'Utilisateur_Base a un Utilisateur_Eleve associé
CREATE TYPE TypeUtilisateur AS ENUM ('prof', 'eleve');

-- Définit le type de signature
-- 'code' -> Signature par code de 6 chiffres
-- 'QR' -> Signature par scan de QR code éphémère
-- 'bluetooth' -> Signature par balise bluetooth
-- 'manuel' -> Signature excusée ou ajoutée manuellement par l'intervenant
CREATE TYPE TypeSignature AS ENUM ('code', 'QR', 'bluetooth', 'manuel');

--------------------------------------------------
-- REFERENCE TABLES
--------------------------------------------------

-- Représente une salle de cours (intemporel)
CREATE TABLE Salle
(
    id INT NOT NULL,
    -- Ex : Home
    nom_batiment VARCHAR(255),
    -- Ex : H-202
    nom_salle VARCHAR(255),

    PRIMARY KEY (id)
);
SELECT create_reference_table('Salle');

SELECT print_notice('-- Created table Salle');

-- Représente une promo groupée par année scolaire
-- (RGPD) Les données enfantsont :
-- - Archivées au bout d'un an (accès interdit, sauf admin)
-- - Supprimées au bout de 5 ans
CREATE TABLE Promotion
(
    id INT,
    -- ex : (ref) FISA-2025
    code VARCHAR(255),
    -- ex: Formation Ingénieur / Statut Apprenti / Année obtention diplôme 2025
    full_name VARCHAR(255),
    -- Année où la donnée est considérée comme "archivée". 
    lock_date TIMESTAMP,
    -- Année où toutes les données de la promotion seront supprimées.
    deletion_date TIMESTAMP,

    PRIMARY KEY (id)
);

SELECT create_reference_table('Promotion');
SELECT print_notice('-- Created table Promotion');

-- Représente un compte utilisateur, fractionné verticalement
-- Répliqué sur tous les nodes, car référencé par le compte de type prof
CREATE TABLE Utilisateur_Base
(
    id INT NOT NULL,
    -- Ex : prof, eleve
    type_utilisateur TypeUtilisateur NOT NULL,
    -- Ex : Logan
    prenom VARCHAR(255),
    -- Ex : TANN
    nom VARCHAR(255),

    PRIMARY KEY (id)
);
SELECT create_reference_table('Utilisateur_Base');
SELECT print_notice('-- Created table Utilisateur_Base');

-- Données propre aux intervenants
-- Un intervenant travaille sur plusieurs promos. Il doit être répliqué sur tous les nodes.
CREATE TABLE Utilisateur_Prof
(
    id INT NOT NULL,
    -- Ex : 50
    salaire_horaire INT DEFAULT 50,
    -- Indique si le prof est intervenant extérieur
    is_externe BOOLEAN DEFAULT FALSE,

    PRIMARY KEY (id)
);
SELECT create_reference_table('Utilisateur_Prof');
SELECT print_notice('-- Created table Utilisateur_Prof');


-- Données uniquement liées à l'authentification du compte utilisateur.
-- Pas besoin de distribuer dans notre cas
-- (RGPD) Les comptes et leurs données doivent :
-- - être désactivées après sortie de formation
-- - être supprimées après 1 an
CREATE TABLE Utilisateur_Account
(
    id INT REFERENCES Utilisateur_Base(id) NOT NULL,
    email VARCHAR(255),
    mdp VARCHAR(255),
    expiration_date TIMESTAMP
);
SELECT create_reference_table('Utilisateur_Account');
SELECT print_notice('-- Created table Utilisateur_Account');

--------------------------------------------------
-- TABLES DISTRIBUEES PAR PROMO
--------------------------------------------------

-- Représente un module pour un semestre de FISE, FISA...
-- Peut être enseignée dans plusieurs classes
CREATE TABLE Matiere
(
    id INT NOT NULL,
    -- Ex : Base de données réparties
    nom_matiere VARCHAR(255),

    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo)
);
SELECT create_distributed_table('Matiere', 'id_sharding_promo');
SELECT print_notice('-- Created table Matiere');


-- Représente une classe d'élèves dans une promo
CREATE TABLE Classe
(
    id INT NOT NULL,
    -- Ex : "M1 APP / Groupe LSI 1"
    nomClasse VARCHAR(255),
    -- Ex : 40 (élèves)
    effectif INT,

    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo)
);
SELECT create_distributed_table('Classe', 'id_sharding_promo');
SELECT print_notice('-- Created table Classe');

-- Données propre aux élèves
CREATE TABLE Utilisateur_Eleve
(
    -- Ex : (ref) Logan
    id INT NOT NULL,
    -- Ex : (ref) M1 APP LSI 1
    id_classe INT,

    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo)
);
SELECT create_distributed_table('Utilisateur_Eleve', 'id_sharding_promo');
SELECT print_notice('-- Created table Utilisateur_Eleve');


-- Lie une matière à une classe
CREATE TABLE Enseignement
(
    id INT NOT NULL,
    -- Ex : les LSI 1
    id_classe INT,
    -- Ex : ont la matière BDD Réparties
    id_matiere INT,
    
    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo)
);
SELECT create_distributed_table('Enseignement', 'id_sharding_promo');
SELECT print_notice('-- Created table Enseignement');

--------------------------------------------------
-- TABLES DISTRIBUEES PAR PROMO ET PAR MOIS
-- Avec compression au bout d'un semestre (5 mois)
--------------------------------------------------

-- Représente un évènement dans le calendrier (émargement nécessaire)
CREATE TABLE CreneauFormation
(
    id INT NOT NULL,
    -- Ex : Base de données réparties (CTD GRP. LSI1)
    label VARCHAR(255),
    -- Ex : 02/05/24 (pour le partitionnement)
    jour TIMESTAMP,
    -- Ex :          14:00
    heure_debut TIMESTAMP,
    -- Ex :          16:00
    heure_fin TIMESTAMP,
    -- Ex : (ref) Bases de données réparties
    id_enseignement INT,
    -- Ex : (ref) H-202
    id_salle INT,

    -- Ex: Interdiction de signer au bout de = 5min,
    expiration_minutes INT DEFAULT 5,
    -- Self explainatory
    code_secret INT,
    -- Id intervenant (qui a le droit d'ajouter manuellement une signature pour les élèves)
    id_prof INT,

    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo, jour)
) PARTITION BY RANGE (jour);
SELECT create_distributed_table('CreneauFormation', 'id_sharding_promo');

SELECT print_notice('-- Created table CreneauFormation');

-- Représente une signature pour un élève
CREATE TABLE Presence
(
    id INT NOT NULL,
    -- Ex : (ref) Logan
    id_utilisateur INT,
    -- Ex : (ref) signe pour le cours de BDDR à 14:00
    id_event INT,
    -- Ex : 02/05/24 (pour le partitionnement)
    jour TIMESTAMP,
    -- Ex : le 02/05/24 à 14:05
    date_signature TIMESTAMP,

    -- Ex : url vers la signature
    signature_png VARCHAR(255),
    -- Ex : code, manuel...
    moyen_signature TypeSignature,

    -- Ex : (ref) FISA-2025
    id_sharding_promo INT REFERENCES Promotion(id),
    PRIMARY KEY (id, id_sharding_promo, jour)
) PARTITION BY RANGE (jour);
SELECT create_distributed_table('Presence', 'id_sharding_promo');
SELECT print_notice('-- Created table Presence');
